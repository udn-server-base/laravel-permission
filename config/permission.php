<?php
/**
 * Permission Package Settings
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/25
 * @since 1.0.0 2018/12/25 6:05 PM init
 * @since 1.1.0 2019/01/14 5:50 PM add projects
 * @since 1.2.0 2020/02/18 11:17 AM remove table name
 */

return [
    "models" => [
        "permission"    => \Samslhsieh\Permission\Models\Permission::class,
        "role"          => \Samslhsieh\Permission\Models\Role::class,
        "project"       => \Samslhsieh\Permission\Models\Project::class,
    ],

    "migrations" => [
        "users" => env("PERMISSION_MIGRATIONS_USERS", false),
    ],
];