<?php
/**
 * Project Interface
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2019/01/14
 * @since 1.0.0 2019/01/14 6:06 PM PM init
 */

namespace Samslhsieh\Permission\Contracts;


use Illuminate\Database\Eloquent\Relations\HasMany;

interface Project
{
//    TODO: comment
    /**
     * A permission can be applied to roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): HasMany;

    /**
     * Find a permission by its name.
     *
     * @param string $name
     * @throws \Samslhsieh\Permission\Exceptions\PermissionDoesNotExist
     * @return Permission
     */
    public static function findByName(string $name): self;

    /**
     * Find a permission by its id.
     *
     * @param int $id
     * @throws \Samslhsieh\Permission\Exceptions\PermissionDoesNotExist
     * @return Permission
     */
    public static function findById(int $id): self;
}