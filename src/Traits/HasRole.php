<?php
/**
 * Your file description
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/27
 * @since 1.0.0 2018/12/27 5:13 PM description
 */

namespace Samslhsieh\Permission\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Samslhsieh\Permission\Contracts\Role;
use Samslhsieh\Permission\Exceptions\RoleDoesNotExist;

trait HasRole
{
    private $roleClass;

    /**
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function getRoleClass()
    {
        return $this->roleClass = app(\Samslhsieh\Permission\Models\Role::class);
    }

    /**
     * @param int|string|array|Role $role
     * @return bool
     * @throws RoleDoesNotExist
     */
    public function hasRole($role): bool
    {
        $role = $this->getStoredRole($role);

        if ($role instanceof Role) {
            return $this->roles->contains($role);
        }

        throw new RoleDoesNotExist();
    }

    /**
     * @param int|string|array|Role $role
     * @return bool
     */
    public function checkRole($role): bool
    {
        try {
            return $this->hasRole($role);
        } catch (RoleDoesNotExist $e) {
            return false;
        }
    }

    /**
     * @param mixed ...$roles
     * @return bool
     */
    public function hasAnyRole(...$roles): bool
    {
        if (is_array($roles[0])) {
            $roles = $roles[0];
        }

        foreach ($roles as $role) {
            if ($this->checkRole($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param mixed ...$roles
     * @return bool
     */
    public function hasAllRoles(...$roles): bool
    {
        if (is_array($roles[0])) {
            $roles = $roles[0];
        }

        foreach ($roles as $role) {
            if (! $this->checkRole($role)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Collection
     */
    public function getRoleNames(): Collection
    {
        return $this->roles->pluck('name');
    }

    /**
     * @param mixed ...$roles
     * @return $this
     * @throws RoleDoesNotExist
     */
    public function assignRole(...$roles)
    {
        if (is_array($roles[0])) {
            $roles = $roles[0];
        }

        $roles = collect($roles)
            ->flatten()
            ->map(function ($role) {
                return $this->getStoredRole($role);
            })
            ->filter(function ($role) {
                return $role instanceof Role;
            })
            ->pluck("id");

        $this->roles()->sync($roles, false);
        $this->load('roles');

        return $this;
    }

    /**
     * @param $role
     * @return $this
     * @throws RoleDoesNotExist
     */
    public function removeRole($role)
    {
        $this->roles()->detach($this->getStoredRole($role));
        $this->load('roles');

        return $this;
    }

    /**
     * @param mixed ...$roles
     * @return HasRole
     * @throws RoleDoesNotExist
     */
    public function syncRoles(...$roles)
    {
        $this->roles()->detach();
        return $this->assignRole($roles);
    }

    /**
     * @param Builder $query
     * @param $roles
     * @return Builder
     * @throws RoleDoesNotExist
     */
    public function scopeRole(Builder $query, $roles): Builder
    {
        if ($roles instanceof Collection) {
            $roles = $roles->all();
        }

        if (! is_array($roles)) {
            $roles = [$roles];
        }

        $roles = array_map(function ($role) {
            if ($role instanceof Role) {
                return $role;
            }

            return $this->getStoredRole($role);
        }, $roles);

        return $query->whereHas('roles', function ($query) use ($roles) {
            $query->where(function ($query) use ($roles) {
                foreach ($roles as $role) {
                    $query->orWhere(config('permission.table_names.roles').'.id', $role->id);
                }
            });
        });
    }

    /**
     * @param $roles
     * @return mixed
     * @throws RoleDoesNotExist
     */
    protected function getStoredRole($roles)
    {
        $roleClass = $this->getRoleClass();

        if (is_numeric($roles)) {
            return $roleClass->findById($roles);
        }

        if (is_string($roles)) {
            return $roleClass->findByName($roles);
        }

        if (is_array($roles)) {
            return $roleClass
                ->where("active", true)
                ->whereIn('name', $roles)
                ->get();
        }

        if ($roles instanceof Role) {
            return $roles;
        }

        throw new RoleDoesNotExist();
    }
}
