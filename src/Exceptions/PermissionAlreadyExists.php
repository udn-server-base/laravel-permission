<?php
/**
 * Your file description
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/27
 * @since 1.0.0 2018/12/27 12:34 PM description
 */

namespace Samslhsieh\Permission\Exceptions;

use InvalidArgumentException;

class PermissionAlreadyExists extends InvalidArgumentException
{

}