<?php

namespace Samslhsieh\Permission\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Samslhsieh\Permission\Contracts\Role as RoleContract;
use Samslhsieh\Permission\Exceptions\RoleDoesNotExist;
use Samslhsieh\Permission\Traits\HasPermission;

class Role extends Model implements RoleContract
{
    use HasPermission;

    protected $fillable = [ "name", "label", "active", "memo" ];

    /**
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @param int $id
     * @return Role
     */
    public static function findById(int $id): RoleContract
    {
        $role = static::active()->find($id);

        if (! $role) {
            throw RoleDoesNotExist::withId($id);
        }

        return $role;
    }

    /**
     * @param string $name
     * @return Role
     */
    public static function findByName(string $name): RoleContract
    {
        $role = static::active()->where('name', $name)->first();

        if (! $role) {
            throw RoleDoesNotExist::withName($name);
        }

        return $role;
    }
}
